# 04-use-eslint

- add `console.log("👋 hello world 🌍")`
- commit on `master`
- show quality report in the tabs of the pipeline
- remove `console.log("👋 hello world 🌍")` and create a merge request
- show quality report in the tabs of the pipeline
- show quality report widget in the merge request
- add another `console.log("🖐️ hello world 🎃")` to another line
- commit
- etc...

